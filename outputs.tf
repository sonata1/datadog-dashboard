output "id" {
  description = "The ID of this dashboard."
  value       = var.enabled ? datadog_dashboard_json.dashboard_json[0].id : ""
}

output "url" {
  description = "The URL of the dashboard"
  value       = var.enabled ? datadog_dashboard_json.dashboard_json[0].url: ""
}
