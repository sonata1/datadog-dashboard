terraform {
  required_providers {
    datadog = {
      source = "DataDog/datadog"
    }
  }
}

provider "aws" {
  region = var.region
  alias  = "core_shared"
  assume_role {
    role_arn = "arn:aws:iam::512426816668:role/ReadDatadogKeys"
  }
}

provider "datadog" {
  api_url = "https://api.datadoghq.eu/"
  api_key = data.aws_secretsmanager_secret_version.datadog_api_key.secret_string
  app_key = data.aws_secretsmanager_secret_version.datadog_app_key.secret_string
}
