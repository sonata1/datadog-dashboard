data "aws_secretsmanager_secret_version" "datadog_api_key" {
  secret_id = "arn:aws:secretsmanager:eu-west-2:512426816668:secret:/prod/root/datadog/api_key-1tsRdO"
  provider  = aws.core_shared
}

data "aws_secretsmanager_secret_version" "datadog_app_key" {
  secret_id = "arn:aws:secretsmanager:eu-west-2:512426816668:secret:/prod/root/datadog/app_key-PtwBEP"
  provider  = aws.core_shared
}
