resource "datadog_dashboard_json" "dashboard_json" {
  count = var.enabled ? 1 : 0
  dashboard = var.json_file
}
