variable "region" {
  type        = string
  description = "AWS region"
}

variable "json_file" {
  type        = string
  description = "Path to the datadog dashboard json file"
}

variable "enabled" {
  type        = bool
  description = "Toggles creation of resources and data sources in this repository"
  default     = true
}
