This is the module that creates Datadog dashboard

Example usage:

```
module "datadog-dashboard" {
  source    = <path to this repository, inculding desired tag>
  json_file = file("${path.module}/config-files/datadog_dashboard.json")
  region    = var.region
}
```

`json_file` contains the path to Datadog dashboard json file

Example of `datadog_dashboard.json` file:

```
{
  "title": "Service Management VDIs",
  "description": "Health of the Service Management WorkSpaces",
  "widgets": [
    {
      "definition": {
        "title": "Total WorkSpaces",
        "type": "timeseries",
        "requests": [
          {
            "q": "sum:aws.workspaces.available{account_id:193330866083}"
          }
        ]
      }
    },
    {
      "definition": {
        "title": "WorkSpaces automation lambda errors",
        "type": "timeseries",
        "requests": [
          {
            "q": "avg:aws.lambda.errors{account_id:193330866083,functionname:workspacesautomation}"
          }
        ]
      }
    }
  ],
  "layout_type": "ordered",
  "is_read_only": true,
  "reflow_type": "auto"
```

For more details please check [Terraform registry](https://registry.terraform.io/providers/DataDog/datadog/latest/docs/resources/dashboard_json)

Prerequisites:
AWS account is [integrated with Datadog](https://gitlab.com/genomicsengland/opensource/terraform-modules/datadog-integration)

Limitations:
This module is intended to be used within your GitLab CI/CD pipeline, using
GEL standardized [GitLab runners](https://gitlab.com/genomicsengland/cloud/aws-core/shared/gitlab-runner)

